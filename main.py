from src.messages import *
from src.commands import *


async def main():
    me = await bot.get_me()
    print(me.username)

    await bot.start()
    await bot.run_until_disconnected()

with bot:
    bot.loop.run_until_complete(main())
