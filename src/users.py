def add_participant(new_id):
    users_list.add(new_id)
    with open('files/users.txt', 'w') as file:
        for user_id in users_list:
            file.write(str(user_id) + '\n')


users_list = set()
with open('files/users.txt', 'r') as file:
    for row in file.readlines():
        if row != '':
            users_list.add(int(row.strip()))
