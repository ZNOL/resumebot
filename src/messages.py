from src.users import *
from src.admin import *
from src.keyboards import *
from src.bots_info import bots_list
from src.bot import *
from telethon import events


@bot.on(events.NewMessage)
async def message_reply(event):
    id, text = event.peer_id.user_id, event.raw_text

    if '/start' in text:
        txt = 'Этот бот раскажет вам о компании BotRepository📦'
        template = main_keyboard[::]
        if id in admins:
            template.append([Button.text('🤖Админка')])

        await bot.send_message(id, txt, buttons=template)

        if id not in users_list:
            cur_user = await bot.get_entity(id)

            add_participant(id)
            for admin_id in admins:
                await bot.send_message(admin_id, 'Новый пользователь (`{}`) @{}'.format(id, cur_user.username))

    elif '🛠Заказать бота' == text:
        txt = 'Пришлите заполненный вами файл. С вами свяжутся!'
        template = [
            Button.url('Написать↗️', 'https://t.me/ZNOL4')
        ]
        # await bot.send_file(id, file='files/Check_list.docx', caption=txt, buttons=template)
        await bot.send_message(id, 'Напишите нашему менеджеру', buttons=template)

    elif '🔍Примеры работ' == text:
        txt = ''
        template = []
        for idx, name in enumerate(bots_list):
            txt += '{}. {}\n'.format(idx + 1, name)
            template.append(Button.inline(
                str(idx + 1), 'Show={}'.format(idx)
            ))
        await bot.send_message(id, txt, buttons=template)

    elif '📦О компании' == text:
        txt = '''Компания BotRepository основана не так давно, но уже успела набрать репутацию!
‼️Основные преимущества нашей компании это:‼️
1⃣ Помощь в составлении Технического задания
2⃣ Опытные программисты с многолетним стажем 
3⃣ Личная консультация по поводу бота
4⃣ Приятные цены доступные каждому
5⃣ Отзывчивая служба поддержки
6⃣ Найдём решение любой вашей задачи
7⃣ Возможность улучшение и доработок бота'''
        await bot.send_file(id, file='files/about.jpg', caption=txt)

    elif '📱Контакты' == text:
        txt = 'Почта: botdeveloperr@gmail.com\n' \
              '[👶🏿 Программист1](https://t.me/ZNOL4)\n' \
              '[🧕🏿 Программист2](https://t.me/PoliNaTr255)'
        await bot.send_message(id, txt, buttons=profiles_keyboard, link_preview=False)

    elif '🤖Админка' == text:
        await change_value(False)
        txt = 'Выберите меню'
        template = [
            [Button.inline(f'Список пользователей: {len(users_list)}', 'Список')],
            [Button.inline('✉️Рассылка')]
        ]
        await bot.send_message(id, txt, buttons=template)

    elif id in admins:
        flag = await get_value()
        if flag:
            await change_value(False)
            for user_id in users_list:
                try:
                    await bot.send_message(user_id, text)
                except ValueError:
                    pass