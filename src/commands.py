from os import listdir
from src.users import *
from src.admin import *
from src.bots_info import bots_list
from src.keyboards import *
from src.bot import *
from telethon import events


@bot.on(events.callbackquery.CallbackQuery())
async def command_reply(event):
    id, command = event.original_update.peer.user_id, event.original_update.data.decode('utf-8')

    if 'Show' in command:
        idx = int(command.split('=')[-1])
        directory = 'files/Bots/{}/'.format(bots_list[idx])

        with open(directory + 'info.txt', 'r', encoding='utf-8') as file:
            info = file.read()
        images = [directory + file for file in listdir('files/Bots/{}'.format(bots_list[idx]))
                  if (file.count('.jpg') + file.count('.png')) != 0]

        template = []
        if idx > 0:
            template.append(Button.inline('⬅️Назад', 'Show={}'.format(idx - 1)))
        if idx < len(bots_list) - 1:
            template.append(Button.inline('Вперёд➡️', 'Show={}'.format(idx + 1)))

        if images:
            await bot.send_file(id, file=images)
            await bot.send_message(id, info, buttons=template)
        else:
            await bot.send_message(id, info, buttons=template)

    elif 'Список' == command:
        seq = []
        for user_id in users_list:
            try:
                user = await bot.get_entity(user_id)
                txt = '@{}    (`{}`)\n\n'.format(user.username, user_id)
                seq.append(txt)
            except ValueError:
                pass
        for idx in range(0, len(seq), 20):
            await bot.send_message(id, ''.join(seq[idx: idx + 20]))

    elif '🤖Админка' == command:
        await change_value(False)
        txt = 'Выберите меню'
        template = [
            [Button.inline(f'Список пользователей: {len(users_list)}', 'Список')],
            [Button.inline('✉️Рассылка')]
        ]
        await bot.send_message(id, txt, buttons=template)

    elif '✉️Рассылка' == command:
        await change_value(True)
        await bot.send_message(id, 'Напишите сообщение', buttons=[
            Button.inline('❌Отмена', '🤖Админка')
        ])
