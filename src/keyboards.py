from telethon.tl.types import *
from telethon.tl.custom import *
from telethon.tl.functions import *

main_keyboard = [
    [Button.text('🛠Заказать бота', resize=True), Button.text('🔍Примеры работ')],
    [Button.text('📦О компании'), Button.text('📱Контакты')]
]

profiles_keyboard = [
    [Button.url('👶🏿 Программист1', 'https://t.me/ZNOL4')],
    [Button.url('🧕🏿 Программист2', 'https://t.me/PoliNaTr255')]
]